var dataJson;
var checkId;

function loadJSON(callback) {
    var xobj = new XMLHttpRequest();

    xobj.overrideMimeType("application/json");
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
        } else {}
    };

    xobj.open('GET', 'data/data.json', true);
    xobj.send(null);
};

loadJSON(function (response) {
    dataJson = JSON.parse(response);
});

function createContentCanvas(){
  
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let content = document.getElementById('modalContent');

    modal.style.display = "block";

    span.onclick = function () {
        modal.style.display = "none";
        content.innerHTML = "";
        removeImage();
        removeButton();
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
            content.innerHTML = "";
            removeImage();
            removeButton();
        }
    }
}



function writeContent(id){

    let description = document.getElementById('description');

    if (description) {
        description.outerHTML = " ";
    }

    let content = document.getElementById("modalContent");
    let title = document.createElement('h2');
    let textContent = document.createElement('a');
    
    textContent.id = "modalTextContent"
    
    content.appendChild(title);
    content.appendChild(textContent);

    if (id == 'whatIsOwasp') {
        title.innerText = dataJson.whatIsOwasp.title;
        textContent.innerText = dataJson.whatIsOwasp.content;
        whatIsOwaspContent();
    } else if (id == 'owaspTopTen'){
        title.innerText = dataJson.owaspTopTen.title;
        let list = document.createElement('ul');
        list.id = "topTenList";
        textContent.appendChild(list);
        let i = 0;
        for(i in dataJson.owaspTopTen.content){
            let newElement = document.createElement('li');
            list.appendChild(newElement);
            newElement.id = i;
            newElement.innerText = i;
            newElement.className = "new-element-list-top-ten";
            newElement.addEventListener("click", function(){
                showContentTopTen(this.id);
            });
            newElement.addEventListener("click", function () {
                selectedItem(this.id);
            });
        }
    } else if(id == 'zap') {
        title.innerText = dataJson.zap.title;
        textContent.innerText = dataJson.zap.content;
    } else if (id == 'jenkins') {
        title.innerText = dataJson.jenkins.title;
        textContent.innerText = dataJson.jenkins.content;
    }
}

function showContent(id){
    createContentCanvas();
    writeContent(id);
    loadImage(id);
}

function showContentTopTen(id){

    let description = document.getElementById('description');

    if(description){
       description.outerHTML = " ";
    }

    let content = document.getElementById("modalMainContent");
    let container = document.createElement('div');
    let title = document.createElement('h2');
    let textContent = document.createElement('a');
    let url = document.createElement('iframe');
    let more = document.createElement('a');
    
    content.appendChild(container);
    container.id = "description";
    container.style.display = "grid";
    container.appendChild(title);
    container.appendChild(textContent);
    container.appendChild(url);
    container.appendChild(more);

    url.style.border = "none";
    more.className = 'btn-more';
    more.innerText = "more information";

    if (id == 'injections'){
        title.innerText = dataJson.owaspTopTen.content.injections.title;
        textContent.innerText = dataJson.owaspTopTen.content.injections.content;
        url.src = dataJson.owaspTopTen.content.injections.url;
        more.href = dataJson.owaspTopTen.content.injections.more;

    } else if (id == 'brokenUser'){
        title.innerText = dataJson.owaspTopTen.content.brokenUser.title;
        textContent.innerText = dataJson.owaspTopTen.content.brokenUser.content;
        url.src= dataJson.owaspTopTen.content.brokenUser.url;
        more.href = dataJson.owaspTopTen.content.brokenUser.more;

    } else if (id == 'sensitiveDataExposure'){
        title.innerText = dataJson.owaspTopTen.content.sensitiveDataExposure.title;
        textContent.innerText = dataJson.owaspTopTen.content.sensitiveDataExposure.content;
        url.src= dataJson.owaspTopTen.content.sensitiveDataExposure.url;
        more.href = dataJson.owaspTopTen.content.sensitiveDataExposure.more;

    } else if (id == 'xmlExternalEntities'){
        title.innerText = dataJson.owaspTopTen.content.xmlExternalEntities.title;
        textContent.innerText = dataJson.owaspTopTen.content.xmlExternalEntities.content;
        url.src= dataJson.owaspTopTen.content.xmlExternalEntities.url;
        more.href = dataJson.owaspTopTen.content.xmlExternalEntities.more;

    } else if (id == 'brokenAccessControl'){
        title.innerText = dataJson.owaspTopTen.content.brokenAccessControl.title;
        textContent.innerText = dataJson.owaspTopTen.content.brokenAccessControl.content;
        url.src= dataJson.owaspTopTen.content.brokenAccessControl.url;
        more.href = dataJson.owaspTopTen.content.brokenAccessControl.more;

    } else if (id == 'securityMissconfiguration'){
        title.innerText = dataJson.owaspTopTen.content.securityMissconfiguration.title;
        textContent.innerText = dataJson.owaspTopTen.content.securityMissconfiguration.content;
        url.src= dataJson.owaspTopTen.content.securityMissconfiguration.url;
        more.href = dataJson.owaspTopTen.content.securityMissconfiguration.more;

    } else if (id == 'insecureDeserializaton'){
        title.innerText = dataJson.owaspTopTen.content.insecureDeserializaton.title;
        textContent.innerText = dataJson.owaspTopTen.content.insecureDeserializaton.content;
        url.src= dataJson.owaspTopTen.content.insecureDeserializaton.url;
        more.href = dataJson.owaspTopTen.content.insecureDeserializaton.more;

    } else if (id == 'usingComponentsWithKnownVulnerabilities'){
        title.innerText = dataJson.owaspTopTen.content.usingComponentsWithKnownVulnerabilities.title;
        textContent.innerText = dataJson.owaspTopTen.content.usingComponentsWithKnownVulnerabilities.content;
        url.src= dataJson.owaspTopTen.content.usingComponentsWithKnownVulnerabilities.url;
        more.href = dataJson.owaspTopTen.content.usingComponentsWithKnownVulnerabilities.more;

    } else if (id == 'insufficientLoggingAndMonitoring'){
        title.innerText = dataJson.owaspTopTen.content.insufficientLoggingAndMonitoring.title;
        textContent.innerText = dataJson.owaspTopTen.content.insufficientLoggingAndMonitoring.content;
        url.src= dataJson.owaspTopTen.content.insufficientLoggingAndMonitoring.url;
        more.href = dataJson.owaspTopTen.content.insufficientLoggingAndMonitoring.more;

    } else if (id == 'crossSiteScripting'){
        title.innerText = dataJson.owaspTopTen.content.crossSiteScripting.title;
        textContent.innerText = dataJson.owaspTopTen.content.crossSiteScripting.content;
        url.src = dataJson.owaspTopTen.content.crossSiteScripting.url;
    }
}

function selectedItem(id){
    clearStyle();
    let element = document.getElementById(id);
    element.style.fontWeight = "bold";
    element.style.color = "#00AB84";
    element.style.textDecoration = "underline";
}

function clearStyle(){
    let elements = document.getElementsByClassName('new-element-list-top-ten');
    for (i in elements){
        if (i < 10){
            elements[i].style.fontWeight = "normal";
            elements[i].style.color = "white";
            elements[i].style.textDecoration = "none";
        }
    }
}

function whatIsOwaspContent(){
    let content = document.getElementById('modalTextContent');
    let listHead = document.createElement('h2');
    let list = document.createElement('ul');

    content.appendChild(listHead);
    content.appendChild(list);

    
    listHead.className = "list-head";
    list.className = "wio-list";
    listHead.innerText = "tools:";

    for (i in dataJson.whatIsOwasp.tools) {
        let listElement = document.createElement('li');
        let iTxt = document.createElement('a');
        list.appendChild(listElement);
        listElement.appendChild(iTxt);
        listElement.className = "wio-list-element";
        iTxt.className = "new-element-list-top-ten noneTxtDec"
        iTxt.innerText = i.replace(/_/g, ' ');
        let s = [];
        let index = 0;
        s[index] = String(i);
        iTxt.href = eval("dataJson.whatIsOwasp.tools." + s[index++] + ".url");
        };

}

function loadImage(id){
    let main = document.getElementById('modalMainContent');

    if (id != "owaspTopTen"){
        let img = document.createElement('img');
        main.appendChild(img);
        img.className = "img-desc";
        img.id = "imgDesc";


        if (id == "whatIsOwasp"){
           img.src = dataJson.whatIsOwasp.url;
        } else if (id == "zap") {
           img.src = dataJson.zap.url;
        } else if (id =="jenkins") {
           img.src = dataJson.jenkins.url;
           let fwd = document.createElement('a');
           let demo = document.createElement('btn');
           let btnCont = document.createElement('div');
           
           main.appendChild(btnCont);
           btnCont.appendChild(demo);
           btnCont.appendChild(fwd);

           btnCont.id = "btnCont";
           btnCont.className = "btn-cont-auto-menu";
           demo.innerText = "zap flow";
           demo.className = "btn-more automation-menu";
           demo.onclick = function(){
               window.open('templates/zap-flow.html');
           };
           fwd.innerText = "more info";
           fwd.className = "btn-more automation-menu";
           fwd.id = "btnMoreInfo";
           fwd.href = dataJson.jenkins.urlFwd;
        }
    }

}

function removeImage(){
    if (document.getElementById('imgDesc')){
        let parent = document.getElementById('modalMainContent');
        let children = document.getElementById('imgDesc');
        parent.removeChild(children);
    }
}

function removeButton(){
    if (document.getElementById('btnMoreInfo')) {
       let parent = document.getElementById('modalMainContent');
       let children = document.getElementById('btnCont');
       parent.removeChild(children);
    }
}

function changeStyle(id){
    let parent = document.getElementById('content');
    let child = document.getElementById(id);

    if (id == "owasp") {
        parent.style.gridTemplateColumns = ".3fr .25fr .25fr .25fr";
        child.style.background = "#00AB84";
        child.style.fontWeight = "bold";
        child.style.fontSize = "1.1em";
        child.style.transition = ".8s";
        document.getElementById('topTen').style.fontSize = ".8em"
        document.getElementById('zap').style.fontSize = ".8em"
        document.getElementById('automation').style.fontSize = ".8em"
        document.getElementById('topTen').style.background = "#474747"
        document.getElementById('zap').style.background = "#474747"
        document.getElementById('automation').style.background = "#474747"

    } else if (id == "topTen") {
        parent.style.gridTemplateColumns = ".25fr .3fr .25fr .25fr";
        child.style.background = "#00AB84";
        child.style.fontWeight = "bold";
        child.style.fontSize = "1.1em";
        child.style.transition = ".8s";
        document.getElementById('owasp').style.fontSize = ".8em"
        document.getElementById('zap').style.fontSize = ".8em"
        document.getElementById('automation').style.fontSize = ".8em"
        document.getElementById('owasp').style.background = "#474747"
        document.getElementById('zap').style.background = "#474747"
        document.getElementById('automation').style.background = "#474747"
    } else if (id == "zap") {
        parent.style.gridTemplateColumns = ".25fr .25fr .3fr.25fr";
        child.style.background = "#00AB84";
        child.style.fontWeight = "bold";
        child.style.fontSize = "1.1em";
        child.style.transition = ".8s";
        document.getElementById('owasp').style.fontSize = ".8em"
        document.getElementById('topTen').style.fontSize = ".8em"
        document.getElementById('automation').style.fontSize = ".8em"
        document.getElementById('owasp').style.background = "#474747"
        document.getElementById('topTen').style.background = "#474747"
        document.getElementById('automation').style.background = "#474747"
    } else if (id == "automation") {
        parent.style.gridTemplateColumns = ".25fr .25fr .25fr.3fr";
        child.style.background = "#00AB84";
        child.style.fontWeight = "bold";
        child.style.fontSize = "1.1em";
        child.style.transition = ".8s";
        document.getElementById('owasp').style.fontSize = ".8em"
        document.getElementById('topTen').style.fontSize = ".8em"
        document.getElementById('zap').style.fontSize = ".8em"
        document.getElementById('owasp').style.background = "#474747"
        document.getElementById('topTen').style.background = "#474747"
        document.getElementById('zap').style.background = "#474747"
    }
}