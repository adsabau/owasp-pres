var menuCounter = 0;

function fadeDownMenu(){
    let expandIcon = document.getElementById('iconExpand');
    let closeIcon = document.getElementById('iconClose');
    let vanishedElement = document.getElementById('iframeVid');
    let expandElement = document.getElementById('content');
    
    // element.style.height = "65vh";
    // element.style.transition = "2s";
    // console.log(element.style.height);
    if (menuCounter++ % 2 == 0){
        vanishedElement.style.height = "0px";
        vanishedElement.style.transition = "1s";
        expandIcon.style.display = "none";
        closeIcon.style.display = "block";
        showMenuContent(expandElement)

    } else {
        expandIcon.style.display = "block";
        closeIcon.style.display = "none";
        vanishedElement.style.height = "55vh";
        vanishedElement.style.transition = "1s";
        hideMenuContent(expandElement);

    }
}

function showMenuContent(expandElement) {
    expandElement.style.height = "65vh";
    expandElement.style.transition = "1s";

    let cards = document.getElementsByClassName('card')
        for(i in cards){
            if(i < 4){
                cards[i].style.display = "grid";
            }
        }
}

function hideMenuContent(expandElement) {
    expandElement.style.height = "0px";
    expandElement.style.transition = "1s";

    let cards = document.getElementsByClassName('card')
    for (i in cards) {
            if (i < 4) {
                cards[i].style.display = "none";
            }
    }
}

