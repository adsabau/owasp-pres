var counter = 0;

window.onload = initiateAnimation();

document.addEventListener("keydown",  (e) => {
    let key = e.key || e.keyCode;
    if (key === "ArrowRight") {
        counter++;
    } else if (key === "ArrowLeft" && counter > 0) {
        counter--;
    }
        animate(counter)
});

function animate(counterValue){
    if(counterValue == 1){
        loadImage("../resources/logos/lenin.png", "objFive", "lenin");
    } else if(counter == 2){
        createJenkinsJob();
        document.getElementById("steps").innerText = "Jenkins starts a new job for automated tests";
    } else if (counter == 3) {
        loadImage("../resources/logos/zap.png", "objFive", "zap");
        document.getElementById("steps").innerText = "The job will open zap in background";
    } else if (counter == 4){
        document.getElementById("steps").innerText = "Selenium and the broeser of choice for the atomated tests are open";
        loadImage("../resources/logos/selenium.png", "objFour", "obj-logo");
         setTimeout(() => {
            loadImage("../resources/logos/globe.png", "objSix", "obj-logo");
            setTimeout(() => {
                createBrowserList();
            },1000);
         }, 2500);
    } else if (counter == 5){
        document.getElementById("steps").innerText = "Then the automated tests begin, while zap starts the passive scan(ZAP acts as a proxy listening to the requests made by the automated tests)";
        sleniumTests()
    } else if ( counter == 6){
        document.getElementById("steps").innerText = "After the tests end ZAP saves the results in a local db for the follwing active scan, Selenium and the browser will be closed";
        removeSpecifiedChild("objFour", "obj-logo", 1);
        removeSpecifiedChild("objSix", "obj-logo", 2);
        removeChildById("objSix", "browserContainer");
    } else if  ( counter == 7){
        document.getElementById("steps").innerText = "A new jenkins job is triggered after the Selenium tests are done and a new active scan will begin using the results from the persisted zap session";
        loadImage("../resources/logos/globe.png", "objSix", "obj-logo");
        setTimeout(() => {
            createBrowserList();
        }, 1000);
        setTimeout(() => {
            zapRunPenTests();
        }, 2000);
    } else if (counter == 8){
        document.getElementById("steps").innerText = "After the active scan is done the results are sent to jenkins";
        removeSpecifiedChild("objSix", "obj-logo", 2);
        removeChildById("objSix", "browserContainer");
        loadImage("../resources/logos/order.jpg", "chThree", "zap-report");
        setTimeout(()=>{
            let order = document.getElementById('chThree');
            order.style.transition = "2s";
            order.style.marginTop = "0px";
        },1000);
        setTimeout(() => {
            deleteElement("chThree", "zap-report");
        }, 2500);
    } else if ( counter == 9){
        document.getElementById("steps").innerText = "With the received results Jenkins will be able to create a report based on the receive data or a new JIRA task";
        document.getElementById('chOne').style.width = "300px"
        document.getElementById('chTwo').style.width = "300px"
        setTimeout(() => {
            loadImage("../resources/logos/jira.png", "objOne", "jira");
            loadImage("../resources/logos/repport.png", "objThree", "obj-logo");
        }, 1500);
    }
}

function loadImage(src,parentId,childClass){
    let parent = document.getElementById(parentId);
    let child = document.createElement("img");

    parent.appendChild(child);
    
    child.className = childClass;
    child.src = src;
}

function initiateAnimation(){
    loadImage("../resources/logos/jenkins.png", "objTwo", "obj-logo");
    loadImage("../resources/logos/desktop-computer.png", "objFive", "obj-logo");
}

function createJenkinsJob(){
    loadImage("../resources/logos/order.jpg", "chThree", "order");
    setTimeout(()=>{
        let element = document.getElementById("chThree");
        element.style.marginTop = "250px"
        element.style.transition = "1s"
    }, 1000);
    setTimeout(() => {
        deleteElement("chThree", "order");
    }, 2500);
}

function deleteElement(parentId, childClass){
    let parent = document.getElementById(parentId);
    let child = document.getElementsByClassName(childClass)[0];
    parent.removeChild(child);
}

function createBrowserList(){
    parent = document.getElementById("objSix");
    child = document.createElement("div");

    parent.appendChild(child);
    child.className = "browser-container";
    child.id = "browserContainer";
        
    let chrome = document.createElement("div");
    let firefox = document.createElement("div");
    let edge = document.createElement("div");

    child.appendChild(chrome);
    child.appendChild(firefox);
    child.appendChild(edge);


    chrome.id = "chrome";
    firefox.id = "firefox";
    edge.id = "edge";

    loadImage("../resources/logos/chrome.svg", "chrome", "browswer");
    loadImage("../resources/logos/firefox.png", "firefox", "browswer");
    loadImage("../resources/logos/edge.svg", "edge", "browswer");
}

function sleniumTests(){
    seleniumToWeb();
    setTimeout(() => {
        webToSelenium();
    }, 5500);
    setTimeout(() => {
        seleniumToWeb();
    }, 10000);
    setTimeout(() => {
        webToSelenium();
    }, 15500);
}

function seleniumToWeb(){
    loadImage("../resources/logos/pack.png", "chFour", "testPack");
    setTimeout(() => {
        let element = document.getElementById("chFour");
        element.style.marginLeft = "250px"
        element.style.transition = "1s"
    }, 1000);
    setTimeout(() => {
        deleteElement("chFour", "testPack");
    }, 2500);
    setTimeout(() => {
        loadImage("../resources/logos/pack.png", "chFive", "testPack");
    }, 3000);
    setTimeout(() => {
        let element = document.getElementById("chFive");
        element.style.marginLeft = "250px"
        element.style.transition = "1s"
    }, 3500);
    setTimeout(() => {
        deleteElement("chFive", "testPack");
    }, 4500);
}

function webToSelenium(){
    loadImage("../resources/logos/pack.png", "chFive", "testPack");
    setTimeout(() => {
        let element = document.getElementById("chFive");
        element.style.marginLeft = "0px"
        element.style.transition = "1s"
    }, 0);
    setTimeout(() => {
        deleteElement("chFive", "testPack");
    }, 1000);
    setTimeout(() => {
        loadImage("../resources/logos/pack.png", "chFour", "testPack");
    }, 2000);
    setTimeout(() => {
        let element = document.getElementById("chFour");
        element.style.marginLeft = "0px"
        element.style.transition = "1s"
    }, 2500);
    setTimeout(() => {
        deleteElement("chFour", "testPack");
    }, 3500);

}
function removeSpecifiedChild(parentId, childClass,childIndex) {
    let parent = document.getElementById(parentId);
    let child = document.getElementsByClassName(childClass)[childIndex];
    console.log(parent)
    console.log(child)
    parent.removeChild(child);
}
function removeChildById(parentId, childId) {
    let parent = document.getElementById(parentId);
    let child = document.getElementById(childId);
    parent.removeChild(child);
}

function zapRunPenTests(){
    loadImage("../resources/logos/zap-pack.png", "chFive", "zapPack");
    setTimeout(() => {
        let element = document.getElementById("chFive");
        element.style.marginLeft = "250px"
        element.style.transition = "1s"
    }, 1000);
    setTimeout(() => {
        deleteElement("chFive", "zapPack");
    }, 2000);
    setTimeout(() => {
        loadImage("../resources/logos/pack.png", "chFive", "zapPack");
    }, 2500);
    setTimeout(() => {
        let element = document.getElementById("chFive");
        element.style.marginLeft = "0px"
        element.style.transition = "1s"
    }, 3000);
    setTimeout(() => {
        deleteElement("chFive", "zapPack");
    }, 4000);

}