var step = 0;

setInterval(function () {
    if(step <= 31){animation()}
}, 1300);

function animation() {
    
    step++;

    if (step == 1) {
        showLenin();
    } else if (step == 2){
        createPackage();
    } else if (step == 3){
        transition();       
    } else if (step == 4){
        removeReq();
        document.getElementById('zap').style.display = "block";
    } else if (step == 5){
        document.getElementById('selenium').style.display = "block";
    } else if (step == 6){
        openBrowser();
    } else if (step == 7){
        seleniumHTTP();
    } else if (step == 8){
        sendRequest();
    } else if (step == 9) {
        removeReq();
    } else if (step == 10) {
        createZapUnmodifyReq();
    } else if (step == 11) {
        sendZapUnmodifyRequest();
    } else if (step == 12) {
        removeReq();
    } else if (step == 13) {
        createZapUnmodifyResp();
    } else if (step == 14) {
        sendZapUnmodifyResp();
    } else if (step == 15) {
        removeReq();
    } else if (step == 16) {
        document.getElementById('selenium').style.display = "none";
        document.getElementById('globe').style.display = "none";
        document.getElementsByClassName('browsers')[0].style.display = "none";
    } else if (step == 17) {
        createPackage();
    } else if (step == 18) {
        transition();
    } else if (step == 19) {
        removeReq();
    } else if (step == 20) {
        openBrowser();
    } else if (step == 21) {
        sendZapModifyRequest();
    } else if (step == 22) {
        sendZapUnmodifyRequest();
    } else if (step == 23) {
        removeReq();
    } else if (step == 24) {
        createZapUnmodifyResp();
    } else if (step == 25) {
        sendZapUnmodifyResp();
    } else if (step == 26) {
        removeReq();
    } else if (step == 27) {
        createRespForJenkins();
    } else if (step == 28) {
        transitionToJenkins();
    } else if (step == 29) {
        removeReq();
    } else if (step == 30) {
        document.getElementById('repport').style.display = "block";
    } else if (step == 31) {
        document.getElementById('jira').style.display = 'block';
    }
}

function showLenin(){
    let lenin = document.getElementById('leninPic');
    lenin.style.display = "block";
}

function createPackage(){
    let parent = document.getElementById('content');
    let child = document.createElement('img');

    parent.appendChild(child);

    child.src = "../resources/logos/order.jpg";
    child.id = "send-req";
    child.style.height = "50px";
    child.style.position = "fixed";
    child.style.left = "50vw";
    child.style.top = "34vh";
    child.style.transition = "1s";
}

function transition(){
    let child = document.getElementById('send-req');
    child.style.top = "50vh";
}

function removeReq(){
   let child = document.getElementById('send-req');
   let parent = document.getElementById('content');

   parent.removeChild(child);
}
function openBrowser(){
    let globe = document.getElementById('globe');
    let browsers = document.getElementsByClassName('browsers');
    let browser = document.getElementsByClassName('browser');

    globe.style.display = "grid";
    browsers[0].style.display = "grid";
    browser[0].style.display = "block";
    browser[1].style.display = "block";
    browser[2].style.display = "block";
}

function seleniumHTTP(){
    let parent = document.getElementById('content');
    let child = document.createElement('img');

    parent.appendChild(child);

    child.src = "../resources/logos/pack.png";
    child.id = "send-req";
    child.style.height = "50px";
    child.style.position = "fixed";
    child.style.left = "21vw";
    child.style.top = "65vh";
    child.style.transition = "1s";
}

function sendRequest(){
    let child = document.getElementById('send-req');
    child.style.left = "36vw";
}

function createZapUnmodifyReq() {
    let parent = document.getElementById('content');
    let child = document.createElement('img');

    parent.appendChild(child);

    child.src = "../resources/logos/pack.png";
    child.id = "send-req";
    child.style.height = "50px";
    child.style.position = "fixed";
    child.style.left = "61vw";
    child.style.top = "65vh";
    child.style.transition = "1s";
}

function sendZapUnmodifyRequest() {
    let child = document.getElementById('send-req');
    child.style.left = "71vw";
}

function createZapUnmodifyResp() {
    let parent = document.getElementById('content');
    let child = document.createElement('img');

    parent.appendChild(child);

    child.src = "../resources/logos/pack.png";
    child.id = "send-req";
    child.style.height = "50px";
    child.style.position = "fixed";
    child.style.left = "71vw";
    child.style.top = "65vh";
    child.style.transition = "1s";
}

function sendZapUnmodifyResp() {
    let child = document.getElementById('send-req');
    child.style.left = "61vw";
}

function sendZapModifyRequest(){
    let parent = document.getElementById('content');
    let child = document.createElement('img');

    parent.appendChild(child);

    child.src = "../resources/logos/zap-pack.png";
    child.id = "send-req";
    child.style.height = "50px";
    child.style.position = "fixed";
    child.style.left = "61vw";
    child.style.top = "65vh";
    child.style.transition = "1s";
    child.style.background = 'rgba(51,51,51,.5)';
    child.style.borderRadius = "3px"
}
function createRespForJenkins() {
    let parent = document.getElementById('content');
    let child = document.createElement('img');

    parent.appendChild(child);

    child.src = "../resources/logos/order.jpg";
    child.id = "send-req";
    child.style.height = "50px";
    child.style.position = "fixed";
    child.style.left = "50vw";
    child.style.top = "50vh";
    child.style.transition = "1s";
}

function transitionToJenkins(){
    let child = document.getElementById('send-req');
    child.style.top = "34vh";
}

